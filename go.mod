module golang-build-case

go 1.13

require (
	github.com/dustin/go-broadcast v0.0.0-20211018055107-71439988bd91
	github.com/gin-gonic/gin v1.5.0
)
